var CategoryController = {
    init: function () {
        CategoryController.registerEvent();
        //SU KIEN BAT MODAL XEM CHI TIET CATEGORY
        $(document).on('click', '.cat-item', function () {
            let id = $(this).find('.category-id').text();
            CategoryController.showFormModal(parseInt(id));
        });
        //SU KIEN BAT MODAL THEM MOI CATEGORY
        $('.btn-add').click(function (e) {
            e.preventDefault();
            CategoryController.showFormModal();
        });
        //SU KIEN CHO PHEP EDIT FORM CATEGORY
        $('.btn-edit').click(function (e) {
            e.preventDefault();
            $("#form-category :input").prop("disabled", false);
            $('.btn-save').prop("disabled", false);
        });
        //SU KIEN XOA CATEGORY
        $('.btn-delete').click(function (e) {
            e.preventDefault();
            let id = $('.input-id').val();
            if (parseInt(id) > 0) {
                if (confirm('Are you sure? this action will also delete its subcategory!')) {
                    CategoryController.deleteCategoryById(id);
                }
            }
        });
        //SU KIEN SUBMIT FORM
        $('.btn-save').click(function (e) {
            e.preventDefault();
            var edit = $('.input-id').val();
            if (edit > 0) {
                CategoryController.saveData('PUT', '/api/categories/' + $('.input-id').val());
            } else {
                CategoryController.saveData('POST', '/api/categories');
            }

        });
    },
    registerEvent: function () {
        this.loadData();
    },
    //FUNCTION SUBMIT DU LIEU
    saveData: function (type, url) {
        var notyf = new Notyf();
        var obj = {
            id: $('.input-id').val(),
            name: $('#input-name').val(),
            meta_title: $('#input-title').val(),
            meta_keyword: $('#input-keyword').val(),
            meta_description: $('#input-description').val(),
            parent_id: $('#input-parent_id').val(),
            status: $('.input-status').prop('checked') ? 1 : 0
        };
        console.log(obj);
        $.ajax({
            type: type,
            url: strUrl + url,
            data: obj,
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    CategoryController.loadData();
                    $('#categoryDetail').modal('hide');
                    notyf.success(response.message);
                } else {
                    notyf.error({
                        message: response.message,
                        duration: 9000,
                        dismissible: true
                    })
                }
            }
        });
    },
    //FUNCTION TAI ROW TABLE
    loadTable: function (data, c = '') {
        for (let item of data) {
            moment(item.updated_at).format('MM/DD/YYYY');
            let row = `<tr class="cat-item">
                            <td class="fw-bold category-id">${item.id}</td>
                            <td>
                            `+ c + `${item.name}
                            </td>
                            <td>
                             
                            </td>
                            <td>
                                <span class="badge ${item.status ? 'bg-success' : 'bg-danger'}">
                                    ${item.status ? 'Enable' : 'Disable'}
                                </span>
                            </td>
                            <td>
                                ${moment(item.updated_at).format('DD/MM/YYYY')}
                            </td>
                        </tr>`;
            $(".category-tbl").append(row);
            if (item.childs != null) {
                var chi = item.childs;
                CategoryController.loadTable(chi, c + ' -- ');
            }
        }
    },
    //FUNCTION TAI OPTION CATEGORY
    loadCategoryOption: function (data, c = '') {

        for (let item of data) {
            let option = `<option value="${item.id}"> ` + c + `${item.name}</option>`;
            $("#input-parent_id").append(option);
            if (item.childs != null) {
                var chi = item.childs;
                CategoryController.loadCategoryOption(chi, c + ' -- ');
            }
        }
    },
    // FUNCTION SHOW FORM THEM MOI HOAC EDIT
    showFormModal: function (id) {
        if (id > 0) {
            $.ajax({
                type: "Get",
                url: strUrl + "/api/categories/" + id,
                success: function (response) {
                    if (response.code == 200) {
                        let cat = response.data;
                        $('.btn-save').prop("disabled", true);
                        $("#form-category :input").prop("disabled", true);
                        $('.text-id').text(cat.id);
                        $('.input-id').val(cat.id);
                        $('#input-name').val(cat.name);
                        $('#input-title').val(cat.meta_title);
                        $('#input-keyword').val(cat.meta_keyword);
                        $('#input-description').val(cat.meta_description);
                        $("#input-parent_id option[value=" + cat.parent_id + "]").prop('selected', true);
                        $("#input-parent_id select").val(cat.id).change();
                        $('.modal-title').text('Category_#');
                        $('.btn-edit').removeClass("invisible");
                        $('.btn-delete').removeClass("invisible");
                    }
                }
            });
        } else {
            $('.btn-save').prop("disabled", false);
            $("#form-category :input").prop("disabled", false);
            $('.text-id').text('');
            $('.input-id').val('');
            $('#input-name').val('');
            $('#input-title').val('');
            $('#input-keyword').val('');
            $('#input-description').val('');
            $("#input-parent_id option[value='0']").prop('selected', true);
            $("#input-parent_id select").val('').change();
            $('.modal-title').text('Add new Category');
            $('.btn-edit').addClass("invisible");
            $('.btn-delete').addClass("invisible");
        }
        $('#categoryDetail').modal('show');
    },
    // FUNCTION TAI DU LIEU
    loadData: function () {
        $.ajax({
            type: "GET",
            url: strUrl + "/api/categories",
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    $(".category-tbl").empty();
                    $("#input-parent_id").empty();
                    $("#input-parent_id").append('<option value="0">Main category</option>');
                    CategoryController.loadTable(response.data);
                    CategoryController.loadCategoryOption(response.data);
                }
            },
        });
    },
    //FUNCTION XOA CATEGORY THEO ID
    deleteCategoryById: function (category_id) {
        var notyf = new Notyf();
        $.ajax({
            type: "DELETE",
            url: strUrl + '/api/categories/' + category_id,
            data: {
                '_token': $('meta[name=csrf-token]').attr('content')
            },
            success: function (response) {
                if (response.code == 200) {
                    CategoryController.loadData();
                    $('#categoryDetail').modal('hide');
                    notyf.success(response.message);
                }
                else if (response.code == 404) {
                    notyf.error({
                        message: response.message,
                        duration: 9000
                    })
                } else {
                    notyf.error({
                        message: response.message,
                        duration: 9000,
                        dismissible: true
                    })
                }
            }
        });
    },
};
CategoryController.init();
