<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','slug','status','parent_id','meta_title','meta_keyword','meta_description','deleted_at'];
    
    public function saveData($request){
        $Category = Category::create([
            'name' => $request['name'],
            'slug' => Str::slug($request['name']),
            'status' =>$request['status'],
            'parent_id' => $request['parent_id'],
            'meta_title' => $request['meta_title'],
            'meta_keyword' => $request['meta_keyword'],
            'meta_description' => $request['meta_description']
        ]);
        return $Category;
    }

    public function childs(){
       return $this->hasMany(Category::class,'parent_id', 'id');
    }
}
