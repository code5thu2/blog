<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Category;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        'name' => $name,
        'slug' => Str::slug($name),
        'status' => (bool)random_int(0,1),
        'parent_id' => random_int(1,20),
        'meta_title' => $faker->realText($maxNbChars = 30),
        'meta_keyword' => $faker->realText($maxNbChars = 15),
        'meta_description' => $faker->realText($maxNbChars = 75)
    ];
});
