@extends('layouts.admin')

@section('title', 'Category tab')
@section('style')
<style>
  tr:hover {
    color: #1F2937;
    background-color: #f0bc74;
    cursor: pointer;
  }
</style>
@endsection
@section('content')
<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
<a href="https://blog.haposoft.com/ban-da-su-dung-dung-resouce-api-laravel/">https://blog.haposoft.com/ban-da-su-dung-dung-resouce-api-laravel/</a>
<div style="min-height: 400px;">
  <div class="card border-0 shadow mb-4 mt-5">
    <div class="card-body">
      <div class="col-12 mb-3">
        <button class="btn btn-success btn-add" type="button">Thêm mới</button>
      </div>
      <div class="table-responsive">
        <table class="table table-centered table-nowrap mb-0 rounded">
          <thead class="thead-light">
            <tr>
              <th class="border-0 rounded-start col-1">#</th>
              <th class="border-0 col-4">Name</th>
              <th class="border-0">number of posts</th>
              <th class="border-0">Status</th>
              <th class="border-0">Update at</th>
            </tr>
          </thead>
          <tbody class="category-tbl">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal  fade" id="categoryDetail" tabindex="-1" role="dialog" aria-labelledby="categoryDetail" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span>
          <h2 class="h6 modal-title"></h2>
        </span>
        <span class="text-id"></span>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form class="row g-3 needs-validation" id="form-category" novalidate>
          <input type="hidden" name="id" class="input-id">
          <div class="row">
            <div class="col-md-6">
              <div class="col-12 mt-2">
                <label for="input-name" class="form-label">Name category</label>
                <input type="text" class="form-control input-name" name="name" id="input-name">
              </div>
              <div class="col-12 mt-2">
                <label for="input-parent_id" class="form-label">Belong to</label>
                <select class="form-select" name="parent_id" id="input-parent_id">
                <option value="0">Main category</option>
                </select>
              </div>
              <div class="col-12 mt-2">
                <label for="input-status" class="form-label">Enable</label>
                <div class="form-check form-switch">
                  <input class="form-check-input input-status" type="checkbox" name="status" id="flexSwitchCheckChecked" checked>
                </div>
              </div>
            </div>
            <div class="col-md-6 ">
              <div class="col-12 mt-2">
                <label for="input-title" class="form-label">Meta title</label>
                <input type="text" class="form-control" name="meta_title" id="input-title">
              </div>
              <div class="col-12 mt-2">
                <label for="input-keyword" class="form-label">Meta keyword</label>
                <input type="text" class="form-control" name="meta_keyword" id="input-keyword">
              </div>
              <div class="col-12 mt-2">
                <label for="input-description" class="form-label">Meta description</label>
                <input type="text" class="form-control" name="meta_description" id="input-description">
              </div>
            </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger btn-delete invisible"><i class="fas fa-trash"></i></button>
      <button type="button" class="btn btn-info btn-edit invisible"><i class="fas fa-edit"></i></button>
      <button type="button" class="btn btn-success btn-save"><i class="fas fa-save"></i> Save</button>
      <!-- <button type="button" class="btn btn-link text-gray ms-auto" data-bs-dismiss="modal">Close</button> -->
    </div>
  </div>
</div>
</div>

@endsection
@section('script')
<script src="{!!asset('public/admin/assets/js/CategoryController.js')!!}"></script>
@endsection